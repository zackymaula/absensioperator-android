package com.diskominfokotakediri.absensioperator.activity.assets;

import android.app.Activity;

import com.diskominfokotakediri.absensioperator.BuildConfig;

public class CodeConfig extends Activity{

    //static String ip_main = "http://10.100.200.204/absensi_operator/";
    //static String ip_main = "http://esuket.kedirikota.go.id/absensi_operator/";

    public static final String URL_GET_OPERATOR = BuildConfig.IP_MAIN+"mobile_operator/get_operator/";
    public static final String URL_INSERT_ABSEN = BuildConfig.IP_MAIN+"mobile_operator/insert_absen";

    /*public static final String URL_GET_OPERATOR = ip_main+"mobile_operator/get_operator/";
    public static final String URL_INSERT_ABSEN = ip_main+"mobile_operator/insert_absen";*/
}
